﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    AudioSource audio1;
    AudioSource audio2;
    AudioSource audio3;
    AudioSource audio4;
    AudioSource audio5;

    AudioClip loseClip;
    AudioClip winClip;
    AudioClip buttonClip;
    AudioClip playerBeGetHurtClip;
    AudioClip playerJumpClip;
    AudioClip playerGetHeartClip;
    AudioClip playerFireClip;
    AudioClip enemyGetHurtClip;
    AudioClip enemyDeathClip;

    void Awake()
    {
        audio1 = GetComponent<AudioSource>();
        audio2 = transform.GetChild(0).GetComponent<AudioSource>();
        audio3 = transform.GetChild(1).GetComponent<AudioSource>();
        audio4 = transform.GetChild(2).GetComponent<AudioSource>();
        audio5 = transform.GetChild(3).GetComponent<AudioSource>();

        loseClip = Resources.Load<AudioClip>("Audios/LoseClip");
        winClip = Resources.Load<AudioClip>("Audios/WinClip");
        buttonClip = Resources.Load<AudioClip>("Audios/ButtonClip");
        playerBeGetHurtClip = Resources.Load<AudioClip>("Audios/PlayerBeGetHurtClip");
        playerJumpClip= Resources.Load<AudioClip>("Audios/PlayerJumpClip");
        playerGetHeartClip = Resources.Load<AudioClip>("Audios/PlayerGetHeartClip");
        playerFireClip = Resources.Load<AudioClip>("Audios/PlayerFireClip");
        enemyGetHurtClip = Resources.Load<AudioClip>("Audios/EnemyGetHurtClip");
        enemyDeathClip = Resources.Load<AudioClip>("Audios/EnemyDeathClip");


    }

    void Update()
    {
        if(EventController.Instance.isCanPlayLoseClip){
            PlayLoseClip();
        }
        if(EventController.Instance.isCanPlayerWinClip){
            PlayWinClip();
        }
        if (EventController.Instance.isCanPlayPlayerFireClip){
            PlayPlayerFireClip();
        }
        if(EventController.Instance.isCanPlayPlayerBeGetHurtClip){
            PlayPlayerBeGetHurtClip();

        }
        if (EventController.Instance.isCanPlayPlayerGetHeartClip){
            PlayPlayerGetHeatClip();
        }
        if (EventController.Instance.isCanPlayPlayerJumpClip){
            PlayPlayerJumpClip();
        }
        if (EventController.Instance.isCanPlayButtonClip){
            playerButtonClip();
        }
        if (EventController.Instance.isCanPlayEnemyBeGetHurtClip){
            PlayEnemyGetHurtClip();
        }
        if (EventController.Instance.isCanPlayEnemyDeathClip){
            PlayEnemyDeathClip();
        }
    }


    void PlayEnemyDeathClip(){

        audio1.clip = enemyDeathClip;
        audio1.Play();
        EventController.Instance.isCanPlayEnemyDeathClip = false;

    }

    void PlayEnemyGetHurtClip(){
        audio1.clip = enemyGetHurtClip;
        audio1.Play();
        EventController.Instance.isCanPlayEnemyBeGetHurtClip = false;



    }

    void PlayPlayerFireClip(){


        audio2.clip = playerFireClip;
        audio2.Play();
        EventController.Instance.isCanPlayPlayerFireClip = false;

    }

    void PlayPlayerGetHeatClip(){

        audio3.clip = playerGetHeartClip;
        audio3.Play();
        EventController.Instance.isCanPlayPlayerGetHeartClip = false;


    }




    void PlayPlayerJumpClip(){

        audio4.clip = playerJumpClip;
        audio4.Play();
        EventController.Instance.isCanPlayPlayerJumpClip = false;


    }

    void PlayPlayerBeGetHurtClip(){

        audio4.clip = playerBeGetHurtClip;
        audio4.Play();
        EventController.Instance.isCanPlayPlayerBeGetHurtClip = false;


    }


    void playerButtonClip(){

        audio3.clip = buttonClip;
        audio3.Play();
        EventController.Instance.isCanPlayButtonClip = false;

    }
    void PlayWinClip(){
        audio3.clip = winClip;
        audio3.Play();
        EventController.Instance.isCanPlayerWinClip = false;

    }

    void PlayLoseClip(){
        audio3.clip = loseClip;
        audio3.Play();
        EventController.Instance.isCanPlayLoseClip = false;

    }
}
