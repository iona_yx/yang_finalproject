﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAttack : MonoBehaviour {

    public float bulletAttackValue = 20;



    void OnTriggerEnter2D(Collider2D cols)
    {
        if (cols.transform.tag == "Enemy"){


            cols.GetComponent<EnemyHealth>().EnemyGetHurt(bulletAttackValue);
            Destroy(gameObject);
        }
    }







}
