﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletAttack : MonoBehaviour {

    public int enemyBulletAttackBalue = 1;


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.tag =="Player"){
            col.GetComponent<PlayerHealth>().PlayerGetHurt(enemyBulletAttackBalue);
            Destroy(gameObject);
        }
    }



}
