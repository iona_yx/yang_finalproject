﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    [HideInInspector]
    SpriteRenderer spriteRenderer;
    public float moveSpeed = 2;
    Transform PlayerTrans;
    public float catchDis = 2;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        PlayerTrans = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void FixedUpdate()
    {
        Move();
        if(transform.childCount<=1){
            return;
        }
        if (Vector2.Distance(transform.position, PlayerTrans.position) < catchDis && Mathf.Abs(transform.position.y - PlayerTrans.position.y)<0.5){
            if(PlayerTrans.position.x<transform.position.x){
                //player is at left of the enemy
                spriteRenderer.flipX = false;

            }else if (PlayerTrans.position.x>transform.position.x){
                spriteRenderer.flipX = true;
                //player is at right of the enemy
            }
        }

    }


    void Move()
    {
        if (spriteRenderer.flipX == false){
            transform.Translate(Vector2.left * Time.deltaTime * moveSpeed);
        }
        else{
            transform.Translate(Vector2.right * Time.deltaTime * moveSpeed);
        }
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Player"){
          
            if (transform.childCount==2&&transform.GetChild(1).name == "Explosion")
            {
                col.transform.GetComponent<PlayerHealth>().PlayerGetHurt(1);
                GetComponent<EnemyHealth>().Death();

            }
            else
            {
                col.transform.GetComponent<PlayerHealth>().PlayerGetHurt(1);
            }
        }
    }
}
