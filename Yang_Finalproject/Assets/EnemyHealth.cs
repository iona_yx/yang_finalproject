﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    Text scoreCountText;
    float enemyCurHealth = 0;
    public float enemyFullHealth = 100;


    void Awake (){

        scoreCountText = GameObject.FindGameObjectWithTag("ScoreCountText").GetComponent<Text>();



    }



    void Start(){
        enemyCurHealth = enemyFullHealth;
    }

    public void EnemyGetHurt(float damage){

        enemyCurHealth -= damage;
        if(enemyCurHealth>0){
            EventController.Instance.isCanPlayEnemyBeGetHurtClip = true;
        }else{
            EventController.Instance.isCanPlayEnemyDeathClip = true;
        }
       
       

        if (enemyCurHealth<=0){
            Death();
        }

    }

   


    public void Death(){

        int curCount = int.Parse(scoreCountText.text);
        curCount += 100;
        scoreCountText.text = curCount.ToString();

        GameController.enemyCount--;
        Destroy(gameObject);
    }


}
