﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponAttack : MonoBehaviour {

    GameObject enemyBullet;
    public float fireRate;
    public float fireSpeed = 100;
    SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        enemyBullet = Resources.Load<GameObject>("EnemyBullet");
    }

    void Start()
    {
        InvokeRepeating("EnemyFireBullet", 3, 3);
    }

    void EnemyFireBullet(){
        GameObject g = Instantiate(enemyBullet, transform.position, Quaternion.identity) as GameObject;
        if(spriteRenderer.flipX == false){
            g.GetComponent<Rigidbody2D>().AddForce(Vector2.left * fireSpeed);
        }else{
            g.GetComponent<Rigidbody2D>().AddForce(Vector2.right * fireSpeed);
        }
    }
}
