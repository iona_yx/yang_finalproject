﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    Transform playerTrans;
    Vector3 offSet;



    void Awake()
    {
        playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
    }


    void Start()
    {
        offSet = transform.position - playerTrans.position;
    }


    void LateUpdate()
    {
        transform.position = playerTrans.position + offSet;
    }


}