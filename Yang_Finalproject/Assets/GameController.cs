﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {


    GameObject winPanel;
    GameObject losePanel;
    public static int enemyCount;
    Transform PlayerTrans;
    bool hasDown = false;

    void Awake(){
        winPanel = GameObject.FindGameObjectWithTag("WinPanel");
        losePanel = GameObject.FindGameObjectWithTag("LosePanel");
        PlayerTrans = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Start(){
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        EventController.Instance.isPlayerLose = false;
        EventController.Instance.isPlayerWin = false;
    }

    void Update()
    {
        if(enemyCount<=0){
            if(hasDown){
                return;
            }
            hasDown = true;
            Camera.main.GetComponent<AudioSource>().enabled = false;
            EventController.Instance.isPlayerWin = true;
            PlayerTrans.GetComponent<PlayerController>().enabled = false;
            PlayerTrans.GetComponent<PlayerWeaponAttack>().enabled = false;
            EventController.Instance.isCanPlayerWinClip = true;
        }
        if (EventController.Instance.isPlayerLose){
            losePanel.SetActive(true);
        }
        if (EventController.Instance.isPlayerWin)
        {
            winPanel.SetActive(true);
        }
    }





}
