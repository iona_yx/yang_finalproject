﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    [HideInInspector]
    public int heartCount = 3;
    Image[] heartImages;
    Transform heartPanel;

    void Awake()
    {
        heartPanel = GameObject.FindGameObjectWithTag("HeartPanel").transform;
        heartImages = new Image[heartPanel.childCount];
        for (int i = 0; i < heartPanel.childCount; i++){

            heartImages[i] = heartPanel.GetChild(i).GetComponent<Image>();
        }

    }
    void Update()
    {
        if (transform.position.y <= -6 ){
            PlayerGetHurt(6);
        }
    }


    public void PlayerGetHurt(int damage)
    {
        EventController.Instance.isCanPlayEnemyBeGetHurtClip = true;
        heartCount -= damage;

        if(heartCount>=6){
            heartCount = 6;
        }
        HeartImageChange();
        if(heartCount<=0){
            Camera.main.GetComponent<AudioSource>().enabled = false;
            EventController.Instance.isCanPlayLoseClip = true;
            EventController.Instance.isPlayerLose = true;
            GetComponent<SpriteRenderer>().enabled = false;
            Camera.main.GetComponent<FollowPlayer> ().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().gravityScale = 0;
        }
    }

    void HeartImageChange(){
        switch(heartCount){
            case 0:
               
            case 1:

            case 2:
               
            case 3:

            case 4:

            case 5:
               
            case 6:
                for (int i = 0; i < heartCount; i++)
                {
                    heartImages[i].gameObject.SetActive(true);
                }
                for (int i = heartCount; i < heartImages.Length; i++)
                {
                    heartImages[i].gameObject.SetActive(false);
                }
                break;


        }
    }
}
