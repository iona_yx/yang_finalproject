﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponAttack : MonoBehaviour {

    GameObject bullet;
    public float fireSpeed = 100;
    SpriteRenderer spriteRenderer;

    void Awake()
    {
        bullet = Resources.Load<GameObject>("Bullet");
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.J)){
            EventController.Instance.isCanPlayPlayerFireClip = true;
            FireBullet();
        }


    }

    void FireBullet()
    {
        GameObject g = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
        if(spriteRenderer.flipX == false){
            g.GetComponent<Rigidbody2D>().AddForce(Vector2.right * fireSpeed);
        }
        else{
            g.GetComponent<Rigidbody2D>().AddForce(Vector2.left * fireSpeed);
        }

    }


}
