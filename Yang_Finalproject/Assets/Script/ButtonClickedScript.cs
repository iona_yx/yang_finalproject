﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonClickedScript : MonoBehaviour {

    Button btn;
    void Awake()
    {
        btn = GetComponent<Button>();
    }

    void Start(){
        switch(transform.name){
            case "StartButton":
                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(startButtonClicked);
                break;
            case"ReturnButton":
                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(ReturnButtonClicked);
                break;
            case"RestartButton":
                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(RestartButtonClicked);
                break;

        }
       
    }


    void startButtonClicked(){
        SceneManager.LoadScene("Level1");
        EventController.Instance.isCanPlayButtonClip = true;
    }


    void ReturnButtonClicked(){
        SceneManager.LoadScene("StartScene");
        EventController.Instance.isCanPlayButtonClip = true;
    }


    void RestartButtonClicked(){
        SceneManager.LoadScene("Level1");
        EventController.Instance.isCanPlayButtonClip = true;
    }
}
