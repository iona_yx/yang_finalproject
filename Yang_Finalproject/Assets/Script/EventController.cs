﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventController {

    private static EventController instance;
    public static EventController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new EventController();
            }

            return instance;
        }
    }

    private EventController() {}

    public bool isPlayerLose = false;
    public bool isPlayerWin = false;

    // aodio and sound 
    public bool isCanPlayLoseClip = false;
    public bool isCanPlayerWinClip = false;
    public bool isCanPlayPlayerBeGetHurtClip = false;
    public bool isCanPlayPlayerGetHeartClip = false;
    public bool isCanPlayPlayerJumpClip = false;
    public bool isCanPlayEnemyBeGetHurtClip = false;
    public bool isCanPlayEnemyDeathClip = false;
    public bool isCanPlayPlayerFireClip = false;
    public bool isCanPlayButtonClip = false;




}
