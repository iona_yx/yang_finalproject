﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{


    float x;
    public float moveSpeed = 50;
    public float jumpSpeed = 100;
    SpriteRenderer spriteRenderer;
    Rigidbody2D rig;

    void Awake()
    {
        rig = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate()
    {
        Move();
       
    }

    void Update()
    {
        Jump();
    }

    void Move()
    {
        x = Input.GetAxis("Horizontal");
        if (x < 0)
        {
            spriteRenderer.flipX = true;
            transform.Translate(Vector2.left * Time.deltaTime * moveSpeed);
        }
        else if (x > 0)
        {
            spriteRenderer.flipX = false;
            transform.Translate(Vector2.right * Time.deltaTime * moveSpeed);
        }

    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (Mathf.Abs(rig.velocity.y) < 0.2f)
            {
                rig.AddForce(Vector2.up * jumpSpeed);
                EventController.Instance.isCanPlayPlayerJumpClip = true;
            }
        }
    }
}
