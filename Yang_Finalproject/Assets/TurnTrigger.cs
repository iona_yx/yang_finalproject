﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnTrigger : MonoBehaviour {


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.transform.name == "TurnTrigger"){
            col.transform.parent.GetComponent<SpriteRenderer>().flipX = !col.transform.parent.GetComponent<SpriteRenderer>().flipX;
        }
    }



}
